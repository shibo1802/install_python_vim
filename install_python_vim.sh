#!/bin/bash
function info {
	echo -e "\033[1;36m$1\033[0m"
}

function error {
	echo -e "\033[1;31m$1\033[0m" 1>&2
}

function check_exit()
{
	if [ $? != 0 ]
	then
		error "something nasty happened"
		exit $?
	fi
}

function backup_home_vim_dir {
    info "***********************************************"
    info "* backup ~/.vim/ , ~/.vim --> ~/vim_backup/"
    info "***********************************************"

    if [ -e ~/.vim ]
    then
    	mv ~/.vim ~/.vim_backup
    fi

    mkdir ~/.vim/
}

function backup_home_vimrc_file {
    info "***********************************************"
    info "* backup ~/.vimrc , ~/.vimrc --> ~/vimrc_backup"
    info "***********************************************"
    if [ -e ~/.vimrc ]
    then
    	mv ~/.vimrc ~/.vimrc_backup
    fi
    
    if [ ! -e ./vimrc_python ]
    then
        error "vimrc_python doesn't exist!"
        error "if there is not too much configuration in your /etc/vim/vimrc, you can exec following commands:"
        error "cp /etc/vim/vimrc ./vimrc_python"
        error "$0"
        exit 1
    fi

    cp ./vimrc_python ~/.vimrc

cat <<-EOF | tee -a ~/.vimrc
"############################################################################"
set nu
set hlsearch
set smartindent
set tabstop=4
set ai! 
set autoindent 
set shiftwidth=4
set background=dark

set colorcolumn=80
highlight ColorColumn ctermbg=5
" set cursorline
set cursorcolumn

" foldmethod : manual indent expr syntax diff marker
set fdm=indent
"############################################################################"
EOF

cat <<-EOF | tee -a ~/.vimrc
"
" for suppress "E173: 1 more file to edit"
"
if argc() > 1
    silent blast " load last buffer
    silent bfirst " switch back to the first
endif

EOF
}

function install_vim {
    info "***********************************************"
    info "* install vim"
    info "***********************************************"

    sudo apt-get -y install vim
}

function install_ctags {
    info "***********************************************"
    info "* install ctags"
    info "***********************************************"

    sudo apt-get -y install ctags
}

function install_taglist {
    info "***********************************************"
    info "* install taglist"
    info "***********************************************"

    sudo apt-get -y install vim-scripts
    sudo apt-get -y install vim-addon-manager
    vim-addons install taglist

cat <<-EOF | tee -a ~/.vimrc
"
" taglist
"
let Tlist_Auto_Highlight_Tag=1
let Tlist_Auto_Open=1
let Tlist_Auto_Update=1
let Tlist_Display_Tag_Scope=1
let Tlist_Exit_OnlyWindow=1
let Tlist_Enable_Dold_Column=1
let Tlist_File_Fold_Auto_Close=1
let Tlist_Show_One_File=1
let Tlist_Use_Right_Window=1
let Tlist_Use_SingleClick=1
let Tlist_WinWidth=50
nnoremap <silent> <F8> :TlistToggle<CR>

EOF
}

function install_pydiction {
    info "***********************************************"
    info "* install pydiction"
    info "***********************************************"

    wget http://www.pythonclub.org/_media/python-basic/pydiction-1.2.zip 
    unzip pydiction-1.2.zip
    mkdir -p ~/.vim/after/ftplugin
    mkdir -p ~/.vim/tools/pydiction/complete-dict
    cp pydiction-1.2/python_pydiction.vim   ~/.vim/after/ftplugin
    cp -r pydiction-1.2/complete-dict       ~/.vim/tools/pydiction/complete-dict
    rm pydiction-1.2.zip
    rm -fr pydiction-1.2

cat <<-EOF | tee -a ~/.vimrc
"
" pydiction
"
let g:pydiction_location='~/.vim/tools/pydiction/complete-dict'
set tabstop=4
set shiftwidth=4
set expandtab
set number

EOF
}

function install_vim_pathogen {
    info "***********************************************"
    info "* install vim-pathogen"
    info "***********************************************"

    mkdir -p ~/.vim/autoload ~/.vim/bundle
    git clone https://github.com/tpope/vim-pathogen.git
    check_exit
    cp vim-pathogen/autoload/pathogen.vim ~/.vim/autoload/
    check_exit
    rm -fr vim-pathogen

cat <<-EOF | tee -a ~/.vimrc
"
" vim-pathogen
"
execute pathogen#infect()

EOF
}

function install_nerdtree {
    info "***********************************************"
    info "* install nerdtree"
    info "***********************************************"

    mkdir -p ~/.vim/bundle
    cd ~/.vim/bundle
    git clone https://github.com/scrooloose/nerdtree.git
    cd -

cat <<-EOF | tee -a ~/.vimrc
"
" NERDTree
"
map <F7> :NERDTreeToggle<CR>
imap <F7> <ESC>:NERDTreeToggle<CR>

EOF
}

function install_supertab {
    info "***********************************************"
    info "* install supertab"
    info "***********************************************"

    git clone https://github.com/ervandew/supertab.git
    cp supertab/doc/ ~/.vim/
    cp supertab/plugin/ ~/.vim/
    rm -fr supertab

cat <<-EOF | tee -a ~/.vimrc
"
"SuperTab
"
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview

EOF
}

####################################################
# install winmanager
####################################################
# git clone https://github.com/vim-scripts/winmanager.git
# cp -r winmanager/doc ~/.vim/ 
# cp -r winmanager/plugin ~/.vim/ 
# rm -fr winmanager

function install_minibufexpl {
    info "***********************************************"
    info "* install minibufexpl"
    info "***********************************************"

    git clone https://github.com/fholgado/minibufexpl.vim.git
    cp -r minibufexpl.vim/doc ~/.vim/
    cp -r minibufexpl.vim/plugin ~/.vim/
    rm -fr minibufexpl.vim 

cat <<-EOF | tee -a ~/.vimrc
"
" miniBufExplorer
"
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

EOF
}

function install_vim_autocomplpop {
    info "***********************************************"
    info "* install vim-autocomplpop"
    info "***********************************************"

    wget https://bitbucket.org/ns9tks/vim-autocomplpop/get/tip.zip 
    unzip tip.zip
    cp -r ns9tks*/doc ~/.vim/
    cp -r ns9tks*/plugin ~/.vim/
    cp -r ns9tks*/autoload ~/.vim/
    rm -fr ns9tks* tip.zip

cat <<-EOF | tee -a ~/.vimrc
filetype plugin on
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascr.pt set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete

EOF
}

function install_library_L9 {
    info "***********************************************"
    info "* install library_L9"
    info "***********************************************"

    git clone https://github.com/vim-scripts/L9.git
    cp -r L9/doc ~/.vim/ 
    cp -r L9/plugin ~/.vim/
    cp -r L9/autoload ~/.vim/
    rm -fr L9
}

install_vim
backup_home_vim_dir
backup_home_vimrc_file
install_ctags
install_taglist
install_pydiction
install_vim_pathogen
install_nerdtree
install_supertab
install_minibufexpl
install_vim_autocomplpop
install_library_L9

info "============================================================="
info "vim-python installation completed!"
info "============================================================="


